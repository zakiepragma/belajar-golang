package main

import (
	"fmt"
)

func main() {
	fmt.Println("hello world")
	fmt.Println("Satu = ", 1)
	fmt.Println("Dua = ", 2)
	fmt.Println("Tiga koma lima = ", 3.5)
	fmt.Println("Benar ", true)
	fmt.Println("Salah ", false)
	fmt.Println("Programmer Cinta Sunnah")
	fmt.Println("programmercintasunnah.com")
	fmt.Println(len("Programmer Cinta Sunnah"))
	fmt.Println("programmercintasunnah.com"[7])

	var name string
	name = "Programmer Salafy"
	fmt.Println(name)

	var youtubeName = "Programmer Cinta Sunnah"
	fmt.Println(youtubeName)

	var age = 23
	fmt.Println(age)

	var year uint64 = 2022
	fmt.Println(year)

	chelsea := 1905
	fmt.Println(chelsea)

	var (
		kelahiran    = 1998
		tempat_lahir = "pd merbau"
	)
	fmt.Println(kelahiran)
	fmt.Println(tempat_lahir)

	const firstname = "Muhammad"
	const lastname = "Ahmad"

	fmt.Println(firstname)
	fmt.Println(lastname)

	const (
		klub   = "chelsea fc"
		negara = "indonesia"
		kiblat = "mekkah"
	)

	fmt.Println(klub)
	fmt.Println(negara)
	fmt.Println(kiblat)

	var nameDia = "hafizah"
	var e = nameDia[0]
	var eString = string(e)
	fmt.Println(eString)
}
